import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { addToFavorites, getFavorites, getSeedList, removeFromFavorites } from "../utils";

const maxPerPage = 100;
const fullSeedList = getSeedList();

export function Search() {
    const [favorites, setFavorites] = useState([]);
    const [currentSeedList, setCurrentSeedList] = useState([]);
    const [images, setImages] = useState([]);
    const [searchTerm, setSearchTerm] = useState("");
    const [pages, setPages] = useState(1);
    const [currentPage, setCurrentPage] = useState(1);

    const reset = () => {
        setCurrentSeedList(fullSeedList);
        setCurrentPage(1);
        setPages(Math.ceil(fullSeedList.length / maxPerPage));
        setImages(fullSeedList.slice(0, maxPerPage));
        setFavorites(getFavorites());
    }

    useEffect(() => {
        reset();
    }, []);

    const changePage = page => {
        const start = page * maxPerPage;
        const end = (page + 1) * maxPerPage;
        setCurrentPage(page);
        setImages(currentSeedList.slice(start, end));
    }

    const toggleLike = seed => {
        if (favorites.includes(seed)) {
            removeFromFavorites(seed);
            setFavorites(prev => {
                const dup = [...prev];
                const idx = prev.indexOf(seed);
                dup.splice(idx, 1);
                return dup;
            });
        } else {
            addToFavorites(seed);
            setFavorites(prev => ([...prev, seed]))
        }
    }

    const runSearch = evt => {
        const term = searchTerm.trim().toLowerCase();
        if (term.length === 0) {
            reset();
            return;
        }
        const searchResults = fullSeedList.filter(el => {
            if (parseInt(el.seed) === parseInt(term)) {
                return true;
            }

            for (const word of el.words) {
                if (word.toLowerCase().includes(term)) {
                    return true;
                }
            }

            return false;
        });
        setCurrentSeedList(searchResults);
        setCurrentPage(1);
        setPages(Math.ceil(searchResults.length / maxPerPage));
        setImages(searchResults.slice(0, maxPerPage));
        evt.preventDefault();
    }

    return (
      <>
        <main>
        <div >
            <div>
                <form onSubmit={runSearch}>
                    <input type="text" placeholder="Search the catalog..." value={searchTerm} onChange={evt => setSearchTerm(evt.target.value)} />
                    <input type="submit" value="Search" />
                </form>
            </div>
        </div>
        <div className="pagination">
            Pages:
            {Array.from(Array(pages)).map((_, idx) => ((currentPage === idx + 1) ? (<span key={idx} className="current-page"><b>{idx + 1}</b></span>) : (<button onClick={() => changePage(idx + 1)} className="page-button" key={idx}>{idx + 1}</button>)))}
        </div>
        <div>
            <div className="image-grid">
            {images.map(el => <div className="grid-box" key={el.seed}>
                <Link to={`/details/${el.seed}`}>
                <img width="250" src={`${process.env.PUBLIC_URL}/images/${el.seed}.webp`} alt={el.words.join(", ")} />
                </Link>
                <p>
                    {el.seed}
                    <button onClick={() => toggleLike(el.seed)} className="like-button">{favorites.includes(el.seed) ? "Unlike" : "Like"}</button>
                </p>
                <pre>{el.words.join(", ")}</pre>
            </div>)}
            </div>
        </div>
        <div className="pagination">
            Pages:
            {Array.from(Array(pages)).map((_, idx) => ((currentPage === idx + 1) ? (<span key={idx} className="current-page"><b>{idx + 1}</b></span>) : (<button onClick={() => changePage(idx + 1)} className="page-button" key={idx}>{idx + 1}</button>)))}
        </div>
        </main>
      </>
    );
  }
