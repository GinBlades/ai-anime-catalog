import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { addToFavorites, getFavorites, getSeedList, removeFromFavorites } from "../utils";
const fullSeedList = getSeedList();

export function Favorites() {
    const [favorites, setFavorites] = useState([]);
    const [images, setImages] = useState([]);

    useEffect(() => {
        const storedFavorites = getFavorites();
        setFavorites(storedFavorites);
        setImages(fullSeedList.filter(i => storedFavorites.includes(i.seed)));
    }, []);

    const toggleLike = seed => {
        if (favorites.includes(seed)) {
            removeFromFavorites(seed);
            setFavorites(prev => {
                const dup = [...prev];
                const idx = prev.indexOf(seed);
                dup.splice(idx, 1);
                return dup;
            });
        } else {
            addToFavorites(seed);
            setFavorites(prev => ([...prev, seed]))
        }
    }

    return (
      <>
        <main className="favorites">
            <h1>Favorites</h1>
        <div>
            <div className="image-grid">
            {images.map(el => <div className="grid-box" key={el.seed}>
                <Link to={`/details/${el.seed}`}>
                <img width="250" src={`${process.env.PUBLIC_URL}/images/${el.seed}.webp`} alt={el.words.join(", ")} />
                </Link>
                <p>
                    {el.seed}
                    <button onClick={() => toggleLike(el.seed)} className="like-button">{favorites.includes(el.seed) ? "Unlike" : "Like"}</button>
                </p>
                <pre>{el.words.join(", ")}</pre>
            </div>)}
            </div>
        </div>
        </main>
      </>
    );
  }
