export function Home() {
    return (
      <>
        <main>
          <h2>Welcome to the AI Anime Catalog!</h2>
          <img src={`${process.env.PUBLIC_URL}/images/31799.webp`} alt="Welcome To The Wolfpack" />
        </main>
      </>
    );
  }
