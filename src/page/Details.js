import { useEffect, useState } from "react";
import { useParams } from "react-router";
import imageData from "../image-list.json";

export function Details() {
    let { id } = useParams();

    const [words, setWords] = useState([]);

    useEffect(() => {
      let wordList = [];
      for (const key in imageData) {
        const seed = imageData[key];
        if (parseInt(seed) === parseInt(id)) {
          wordList.push(key);
        }
      }
      setWords(wordList);
    }, [id]);

    return (
      <>
        <main>
            <h1>Seed: {id}</h1>
            <img src={`${process.env.PUBLIC_URL}/images/${id}.webp`} alt={words.join(", ")} />
            <p><b>
            Words:
            </b></p>
            {words.map(word => <pre key={word}>{word}</pre>)}
            <a target="_blank" rel="noreferrer" href={`https://thisanimedoesnotexist.ai/slider.html?seed=${id}`}>View all Creativity Levels</a>
            <div>
              <p>Some things to know if you try to reproduce the seed with the bot:</p>
              <ul>
                <li>We experimented with a lot of special characters and white space. Some may not be easily reproduced with the word listed here.</li>
                <li>We standardized on backslash lowercase t ( <code>\t</code> ) for spaces between words. It may not always work.</li>
                <li>We found out if you put <code>\t</code> after an emoji, nightbot will display the emoji. So that might be needed to reproduce the seed.</li>
              </ul>
            </div>
        </main>
      </>
    );
  }
