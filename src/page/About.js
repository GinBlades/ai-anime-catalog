export function About() {
    return (
      <>
        <main className="about">
            <h2>Why have you done this?</h2>
            <p>During <a href="https://www.twitch.tv/lobosjr">LobosJr's</a> <b>No Stream November</b>, many of us in the re-run chat had fun with the <a href="https://arfa.dev/waifu-ed/editor_d6a3dae.html?seed=31799">Waifu Bot</a>, by <a href="https://twitter.com/arfafax">Arfafax</a>. This bot matches up words with a random seed from the <a href="https://thisanimedoesnotexist.ai/">This Anime Does Not Exist</a> website. Check out that site for more information about how the images are created. This catalog collects the images we matched up with words from the chat in November.</p>
        </main>
      </>
    );
  }
