import imageData from "./image-list.json";

export function getSeedList() {
    const seedList = [];
    for (const key in imageData) {
        const seed = imageData[key];
        const idx = seedList.map(i => i.seed).indexOf(seed);
        if (idx < 0) {
            seedList.push({seed, words: [key]});
        } else {
            seedList[idx].words.push(key);
        }
    }
    return seedList.sort((a, b) => a.seed - b.seed);
}

export function getFavorites() {
    const favorites = localStorage.getItem("favorites");
    if (favorites == null) {
        return [];
    }
    return JSON.parse(favorites);
}

export function addToFavorites(seed) {
    const favorites = getFavorites();
    favorites.push(seed);
    localStorage.setItem("favorites", JSON.stringify(favorites));
}

export function removeFromFavorites(seed) {
    const favorites = getFavorites();
    const idx = favorites.indexOf(seed);
    favorites.splice(idx, 1);
    localStorage.setItem("favorites", JSON.stringify(favorites));
}
