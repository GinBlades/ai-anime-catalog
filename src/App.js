import "./App.scss";

import { Routes, Route, Link } from "react-router-dom";
import { About } from "./page/About";
import { Home } from "./page/Home";
import { Search } from "./page/Search";
import { Details } from "./page/Details";
import { Favorites } from "./page/Favorites";

function App() {
  return (
    <div className="App content">
      <nav className="main-nav">
        <Link to={`/`}>Home</Link>
        <Link to={`/about`}>About</Link>
        <Link to={`/search`}>Search</Link>
        <Link to={`/favorites`}>Favorites</Link>
      </nav>
      <h1>AI Anime Catalog</h1>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="about" element={<About />} />
        <Route path="search" element={<Search />} />
        <Route path="favorites" element={<Favorites />} />
        <Route path="details/:id" element={<Details />} />
      </Routes>
    </div>
  );
}

export default App;
