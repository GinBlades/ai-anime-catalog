# AI Anime Catalog

To update:

1. Get new chat logs with Rechat tool

    ```
    .\RechatTool.exe -D [videoid]
    ```

2. Move logs and read

    ```
    # in WSL
    cd misc
    mv ../../../../Projects/csharp/RechatTool/RechatTool/bin/Debug/net462/*.json ../extra/lobos-chat-logs
    mv ../../../../Projects/csharp/RechatTool/RechatTool/bin/Debug/net462/*.txt ../extra/lobos-chat-logs
    ruby log_reader.rb
    ```

3. In WSL, run `ruby make_catalog.rb > today-list.log`
4. In PowerShell, run `cd imgconv && node convert.js`
5. Copy `anime-output-complete.json` to `image-list.json`

    ```
    cp .\extra\anime-output-complete.json .\src\image-list.json
    ```

6. Commit and push
