require "open-uri"
require "json"
require "fileutils"

data = JSON.parse(File.read("../extra/anime-output-complete.json"))

count = 0

# Originally I would get new images from the thisanimedoesnotexist.ai website.
# At some point I decided to download them all from the available downloads page, so I just copied
# the ones we requested into the catalog directory
data.each do |word, seed|
  count += 1
  path = "../extra/waifu-catalog/#{seed}.png"

  next if File.exist?(path)
  # URI.open("https://thisanimedoesnotexist.ai/results/psi-1.0/seed#{seed}.png") do |image|
  #   File.open(path, "wb") do |file|
  #     file.write(image.read)
  #     puts "Fetched waifu for '#{word}', ##{seed}"
  #   end
  # end
  FileUtils.cp("/mnt/n/Downloads-2021-12-24/all-waifus/seed#{seed}.png", path)
  puts "Fetched waifu for '#{word}', ##{seed}"

  # if count >= 10
  #   sleep 10
  #   count = 0
  # end
end
