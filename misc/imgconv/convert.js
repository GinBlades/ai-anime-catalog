const webp = require("webp-converter");
const fsp = require("fs").promises;

/**
 * Project to convert the png images to webp
 */
async function run() {
    const srcDir = "../../extra/waifu-catalog";
    const destDir = "../../public/images";
    const rawFiles = await fsp.readdir(srcDir);
    const convertedFiles = await fsp.readdir(destDir);
    for (const file of rawFiles) {
        if (!file.endsWith(".png")) {
            continue;
        }
        const seed = file.split(".")[0];
        const match = convertedFiles.find(i => i.startsWith(seed));
        if (match != null) {
            continue;
        }

        const result = await webp.cwebp(`${srcDir}/${file}`, `${destDir}/${seed}.webp`, "-q 80", logging="-v");
        console.log(result);
    }
};

run();
