require "json"

# This script would read the log files and build the JSON output.
# After 11/15, I watched the logs using a bot.
# I used the RechatTool to get earlier logs: https://github.com/jdpurcell/RechatTool/tree/master/RechatTool
# .\RechatTool.exe -D [videoid]
logs = Dir.glob("../extra/lobos-chat-logs/*.txt")

logs.each do |log|
  lines = File.readlines(log.strip)
  # Match @ FuckingDex, here is your cute waifu! https://arfa.dev/waifu-ed/editor_d6a3dae.html?seed=46184

  results = JSON.parse(File.read("../extra/anime-output-complete.json"))

  lines.each do |line|
    sides = line.split(", here is your cute waifu! ")
    if sides.size == 2
      word = sides[0].split("@")[1].strip
      seed = sides[1].split("=")[1].split(" ")[0]
      results[word] = seed
    end
  end


  File.open("../extra/anime-output-complete.json", "w+") do |f|
    f.write(results.to_json)
  end
end
