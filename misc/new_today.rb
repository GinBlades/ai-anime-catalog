require "date"
require "fileutils"

# I wasn't timestamping the images, so this allowed me to create a directory of images to see what was new
today = Date.today.strftime("%Y-%m-%d")

if !File.exist?(today)
  Dir.mkdir(today)
end

list = File.readlines("today-list.log")

list.each do |line|
  parts = line.split(", #")
  seed = parts[1].strip
  FileUtils.cp("../extra/waifu-catalog/#{seed}.png", "#{today}/#{seed}.png")
end
