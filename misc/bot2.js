const fsp = require("fs").promises;
const tmi = require('tmi.js');

/**
 * Only slightly modified from the Twitch bot example to watch the chat for the waifu command.
 */

// Define configuration options
const opts = {
  identity: {
    username: 'username',
    password: 'oauth:token'
  },
  channels: [
    'lobosjr'
  ]
};

// Create a client with our options
const client = new tmi.client(opts);

// Register our event handlers (defined below)
client.on('message', onMessageHandler);
client.on('connected', onConnectedHandler);

// Connect to Twitch:
client.connect();

// Called every time a message comes in
async function onMessageHandler (target, context, msg, self) {
  if (self) { return; } // Ignore messages from the bot

  if (msg.includes("here is your cute waifu")) {
    await fsp.appendFile("chat.log", msg + "\n");
  }
}

// Called every time the bot connects to Twitch chat
function onConnectedHandler (addr, port) {
  console.log(`* Connected to ${addr}:${port}`);
}
